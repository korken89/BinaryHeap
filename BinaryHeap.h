/****************************************************************************
*
* Copyright (C) 2015 Emil Fresk.
* All rights reserved.
*
* This file is part of the SerialPipe library.
*
* GNU Lesser General Public License Usage
* This file may be used under the terms of the GNU Lesser
* General Public License version 3.0 as published by the Free Software
* Foundation and appearing in the file LICENSE included in the
* packaging of this file.  Please review the following information to
* ensure the GNU Lesser General Public License version 3.0 requirements
* will be met: http://www.gnu.org/licenses/lgpl-3.0.html.
*
* If you have questions regarding the use of this file, please contact
* Emil Fresk at emil.fresk@gmail.com.
*
****************************************************************************/

#ifndef _BINARYHEAP_H
#define _BINARYHEAP_H

#include <utility>
#include <vector>
#include <memory>
#include <functional>

using namespace std;

/**
* @brief       A general Item class for the generalized binary heap. Used to
*              keep track of the heap index and to force any derived class to
*              implement the compareTo() function.
*
* @tparam T    The type of the derived class.
*/
template <class T>
class IHeapItem
{
public:
    /**
    * @brief           Compares the current item to another item.
    *
    * @param[in] item  The item to be compared to the current item.
    * @return          Returns true if the current item is of higher priority.
    */
    virtual bool compareTo(const std::shared_ptr<IHeapItem<T>> item) const = 0;

    size_t heapIndex;
};

/**
* @brief       A class for managing a generalized binary heap. The heap has the
*              property of O(log(n)) for adding and removing items and always
*              has the highest priority item at the top.
*
* @tparam T    The item type the heap contains.
*/
template <class T>
class BinaryHeap
{
private:
    /* @brief   The container for the heap. */
    std::vector< std::shared_ptr<IHeapItem<T>> > heap;

    /* @brief   Counter to keep track of the number of items in the heap. */
    size_t currentItemCount = 0;

    /**
    * @brief                   Sorts the selected element in the heap upwards
    *                          until the parent is deemed to have a higher
    *                          priority by the compare function.
    *
    * @param[in] startingIndex The starting index if the sort.
    */
    void sortUp(size_t startingIndex)
    {
        /* Calculate parent index. */
        size_t currentIndex = startingIndex;
        size_t parentIndex = (currentIndex - 1) / 2;

        /* Safeguard check for when the parent index wraps around. */
        while (currentIndex > 0)
        {
            /* Compare and swap if needed, else return. */
            if (heap[currentIndex]->compareTo(heap[parentIndex]))
            {
                /* Swap the indexes and the items. */
                std::swap(heap[currentIndex]->heapIndex, heap[parentIndex]->heapIndex);
                std::swap(heap[currentIndex], heap[parentIndex]);

                /* Recalculate indexes. */
                currentIndex = parentIndex;
                parentIndex = (currentIndex - 1) / 2;
            }
            else
            {
                return;
            }
        }
    }

    /**
    * @brief                   Sorts the selected element in the heap
    *                          downwards until the children are deemed to have
    *                          a lower priority by the compare function.
    *
    * @param[in] startingIndex The starting index if the sort.
    */
    void sortDown(size_t startingIndex)
    {
        /* Calculate child indexes. */
        size_t currentIndex = startingIndex;
        size_t childLeftIndex = (currentIndex * 2) + 1;
        size_t childRightIndex = (currentIndex * 2) + 2;

        /* Temporary variable. */
        size_t swapIndex;

        while (true)
        {
            /* Check so the indexes are within the size of the heap,
            else return. */
            if (childLeftIndex < currentItemCount)
            {
                swapIndex = childLeftIndex;

                if (childRightIndex < currentItemCount)
                {
                    /* Check which of the children has the highest priority. */
                    if (!heap[childLeftIndex]->compareTo(heap[childRightIndex]))
                        swapIndex = childRightIndex;
                }

                /* Check priorities and swap if needed, else return. */
                if (!heap[currentIndex]->compareTo(heap[swapIndex]))
                {
                    /* Swap the indexes and the items. */
                    std::swap(heap[currentIndex]->heapIndex, heap[swapIndex]->heapIndex);
                    std::swap(heap[currentIndex], heap[swapIndex]);

                    currentIndex = swapIndex;
                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }

            /* Recalculate indexes */
            childLeftIndex = (currentIndex * 2) + 1;
            childRightIndex = (currentIndex * 2) + 2;
        }
    }


public:
    /**
    * @brief                       Constructor. Sets the preallocated size of
    *                              the heap.
    *
    * @param[in] heapStartingSize  The preallocated size of the heap.
    */
    BinaryHeap(size_t heapStartingSize)
    {
        heap.reserve(heapStartingSize);
    }

    /**
    * @brief           Adds and sorts in a new item.
    *
    * @param[in] item  The item to be added to the heap.
    */
    void Add(const std::shared_ptr<IHeapItem<T>> item)
    {
        /* Give item an index. */
        item->heapIndex = currentItemCount;

        /* Add an item to the heap and correct the size of the heap. */
        heap.push_back(item);
        currentItemCount += 1;

        /* Sort. */
        sortUp(currentItemCount - 1);
    }

    /**
    * @brief   Removed the highest priority item and sorts the heap.
    *
    * @return  The top item. Return nullptr when the heap is empty.
    */
    std::shared_ptr<T> RemoveTop()
    {
        if (currentItemCount > 0)
        {
            /* Swap head and tail. */
            std::swap(heap.front(), heap.back());

            /* Take out the tail element and remove it from the heap. */
            const auto tmp = heap.back();
            heap.pop_back();

            /* Correct the size of the heap. */
            currentItemCount -= 1;

            /* Sort. */
            sortDown(0);

            /* Return the pointer in the correct type. */
            return std::dynamic_pointer_cast<T>(tmp);
        }
        else
        {
            return nullptr;
        }
    }

    /**
    * @brief            Re-sorts in an item.
    *
    * @param[in] item   The item to be re-sorted.
    */
    void ResortItem(std::shared_ptr<IHeapItem<T>> item)
    {
        /* Let the sorting functions decide if it sorts up or down. */
        sortUp(item->heapIndex);
        sortDown(item->heapIndex);
    }

    /**
    * @brief   Gets the number of items in the heap.
    *
    * @return  The current number of items.
    */
    size_t GetHeapCount() const
    {
        return currentItemCount;
    }
};

#endif
