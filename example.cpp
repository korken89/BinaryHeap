/****************************************************************************
*
* Copyright (C) 2015 Emil Fresk.
* All rights reserved.
*
* This file is part of the SerialPipe library.
*
* GNU Lesser General Public License Usage
* This file may be used under the terms of the GNU Lesser
* General Public License version 3.0 as published by the Free Software
* Foundation and appearing in the file LICENSE included in the
* packaging of this file.  Please review the following information to
* ensure the GNU Lesser General Public License version 3.0 requirements
* will be met: http://www.gnu.org/licenses/lgpl-3.0.html.
*
* If you have questions regarding the use of this file, please contact
* Emil Fresk at emil.fresk@gmail.com.
*
****************************************************************************/

#include <iostream>
#include "BinaryHeap.h"

class MyItem : public IHeapItem<MyItem>
{
public:

    int cost;

    MyItem(int c)
    {
        cost = c;
    }

    bool compareTo(const std::shared_ptr<IHeapItem<MyItem>> item) const
    {
        auto derivedItem = std::dynamic_pointer_cast<MyItem>(item);

        return (cost < derivedItem->cost);
    }
};


int main()
{
    BinaryHeap<MyItem> myHeap(100);

    auto d1 = make_shared<MyItem>(1);
    auto d2 = make_shared<MyItem>(2);
    auto d3 = make_shared<MyItem>(3);
    auto d4 = make_shared<MyItem>(5);

    myHeap.Add(d3);
    myHeap.Add(d4);
    myHeap.Add(d1);
    myHeap.Add(d2);

    d4->cost = 0;
    myHeap.ResortItem(d4);

    auto o = myHeap.RemoveTop();
    o = myHeap.RemoveTop();
    o = myHeap.RemoveTop();
    o = myHeap.RemoveTop();
    o = myHeap.RemoveTop();

    cin.get();

    return 0;
}
